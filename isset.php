<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//isset — Determine if a variable is set and is not NULL

$a = array ('name' =>"aklima", 'hello' => NULL, 'pie' => array('a' => 'apple','c'=>'circle'));

var_dump(isset($a['name']));            // TRUE
var_dump(isset($a['foo']));             // FALSE
var_dump(isset($a['hello']));// FALSE
echo "<pre>";
var_dump($a);
echo "</pre>";
// The key 'hello' equals NULL so is considered unset
// If you want to check for NULL key values then try: 
var_dump(array_key_exists('hello', $a)); // TRUE

// Checking deeper array values
var_dump(isset($a));        // TRUE
var_dump(isset($a['pie']['c']));        // FALSE
var_dump(isset($a['cake']['a']['b']));  // FALSE

