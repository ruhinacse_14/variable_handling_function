<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$var = 0;

// Evaluates to true because $var is empty
if (empty($var)) {
    echo "$var is either 0, empty, or not set at all";
}

echo "<br>";
// Evaluates as true because $var is set
if (isset($var)) {
    echo "$var is set even though it is empty";
}